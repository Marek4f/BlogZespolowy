"""strona URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin
from strona import views as strona_views
from django.contrib.auth import views as auth_views
from registration.backends.simple.views import RegistrationView
from django.conf import settings
from django.conf.urls.static import static

class MyRegistrationView(RegistrationView):
    def get_success_url(self,request, user):
        return '/blog/'
        
urlpatterns = [
	url(r'^$', strona_views.index),
	url(r'^about/$', strona_views.about),  
    url(r'^przepisy/', include('blog.urls')),
	url(r'^admin/', admin.site.urls), 
	url(r'^contact/$', strona_views.contact), 
	url(r'^login/$', auth_views.login, name='login'),
	url(r'^accounts/', include('registration.backends.simple.urls')),
	url(r'^logout/$', strona_views.logout_page),
	url(r'^search/$', strona_views.search),
	url(r'^profil/$', strona_views.profil),    
	url(r'^newpassword/$', strona_views.newpassword),  
    ] 

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
