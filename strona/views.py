#-*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django.conf import settings
from django.utils.decorators import method_decorator
from django.utils.module_loading import import_string
from django.views.decorators.debug import sensitive_post_parameters
from django.http import HttpResponseRedirect
from django.contrib.auth import logout
from blog.models import Post
from blog.forms import *
from django.contrib.auth.models import User
from django.contrib import messages
from django.template.context import RequestContext
from django.contrib.postgres.search import SearchVector
from decimal import Decimal, ROUND_UP


def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')
    
def search(request):
	se = Search()
	postid=[]
	j=0
	if request.method == "POST":
		form = Search(request.POST)
		if form.is_valid():
			szukaj=request.POST.get('search')
			SearchVector('title')
			post= Post.objects.annotate(search=SearchVector('title'),).filter(search=szukaj)
			i=len(post)
			if i>0:
				print Post.objects.values_list('id', flat=True)
				while j<i:
					postidquery=Post.objects.values_list('id', flat=True).filter(title=post[j])
					postid.append(postidquery[0])
					j=j+1
				return render(request, 'search.html', {'se':se, 'post' : post, 'postid' : postid})
			else:
				messages.warning(request, u'Brak wyników')
	return render(request, 'search.html', {'se':se})
    
def index(request):
	tabo=[]
	listao=[]
	listat=[]
	listaid=[]
	listao2=[]
	listat2=[]
	listaid2=[]
	j=0
	postid=Post.objects.values_list('id', flat=True)
	posttytul=Post.objects.values_list('title', flat=True)
	i=len(posttytul)
	while j<i:
		o=Rate.objects.values_list('ocena', flat=True).filter(post=postid[j])
		if sum(o)!=0:
			l=Decimal(sum(o))/Decimal(len(o))
			l=(Decimal(l).quantize(Decimal('.01'), rounding=ROUND_UP))
		else:
			l=0
		listao.append(l)
		listat.append(posttytul[j])
		listaid.append(postid[j])
		j=j+1
	#print listao.index(min(listao))
	j=0
	while i>5:
		listat.pop(listao.index(min(listao)))
		listaid.pop(listao.index(min(listao)))
		listao.pop(listao.index(min(listao)))
		i=len(listat)
	i=len(listat)
	j=False
	new_list=[]
	while listao:
		minimum = listao[0]  # arbitrary number in list 
		for x in listao: 
			if x < minimum:
				minimum = x
		new_list.append(minimum)
		print listao.index(minimum)
		listat2.append(listat[listao.index(minimum)])
		listaid2.append(listaid[listao.index(minimum)])
		listaid.remove(listaid[listao.index(minimum)])
		listat.remove(listat[listao.index(minimum)])
		listao.remove(minimum)
	listao = new_list[::-1]
	listat = listat2[::-1]
	listaid = listaid2[::-1]
	return render(request, 'index.html', { 'listao': listao,'listaid': listaid ,'listat': listat    })

def about(request):
	return render(request, 'about.html')

def login(request):
	return render(request, 'login.html')


def contact(request):
	return render(request, 'contact.html')

def add_comment_to_post(request):
	return render(request, 'add_comment_to_post.html')

def profil(request):
	comment = Comment.objects.values_list('text', flat=True).filter(author=request.user)
	commentid = Comment.objects.values_list('post_id', flat=True).filter(author=request.user)
	return render(request, 'blog/profil.html', { 'comment' : comment, 'commentid' : commentid })
	
def newpassword(request):
	new = PasswordNew()
	if request.method == "POST":
		form = PasswordNew(request.POST)
		if form.is_valid():
			pas=request.POST.get('newpassword')
			pas2=request.POST.get('newpassword2')
			if pas==pas2:
				u = User.objects.get(username__exact=request.user)
				u.set_password(pas)
				u.save()
				return render(request, 'blog/profil.html')
			else:
				messages.warning(request, u'Hasła w obu polach muszą być identyczne!')
				#return render(request, 'newpassword.html', { 'new': new    })	
	return render(request, 'newpassword.html', { 'new': new    })

