from django.contrib import admin
from django.utils.html import format_html
from django.contrib import admin
from .models import Post, Comment, Rate, Category
# Register your models here.
from .models import Post

class PostModelAdmin(admin.ModelAdmin):
	list_display = [ "title", "author","updated", "timestamp"]
	list_display_links = ["title"]
	#list_editable = ["title"]
	list_filter = ["author", "updated", "timestamp"]
	search_fields = ["title", "content", "author"]
	exclude=('author',)
	def save_model(self, request, Post, form, change):
              Post.author = request.user
              Post.save()
              
	class Meta:
		model = Post

admin.site.register(Post, PostModelAdmin)
admin.site.register(Comment)
admin.site.register(Rate)
admin.site.register(Category)
