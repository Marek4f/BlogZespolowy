#-*- coding: utf-8 -*-
from django import forms 
import re
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from registration.forms import RegistrationForm
from .models import Post, Comment, Rate


class RegistrationForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)
    email = forms.EmailField(label='Email')
    password1 = forms.CharField(label='Passwordfffff',
                          widget=forms.PasswordInput())
    password2 = forms.CharField(label='Password (Again)', widget=forms.PasswordInput())

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password1 == password2:
                return password2
        raise forms.ValidationError('Passwords do not match.')

    def clean_username(self):
        username = self.cleaned_data['username']
        if not re.search(r'^\w+$', username):
            raise forms.ValidationError('Username can only contain alphanumeric characters and the underscore.')
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username
        raise forms.ValidationError('Username is already taken.')
        
class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('author', 'text',)
        exclude = ["user"]
        
class PasswordNew(forms.Form):
	newpassword = forms.CharField(label='Hasło', widget=forms.PasswordInput(), required=True)
	newpassword2 = forms.CharField(label=' Powtórz hasło', widget=forms.PasswordInput(), required=True)
	
class Search(forms.Form):
	search = forms.CharField(label='Szukaj',  required=True)

class Oceny(forms.Form):
	lio = (
    ('1', '1'),
    ('2', '2'), ('3', '3'), ('4', '4'),  ('5', '5'), ('6', '6'),('7', '7'),  ('8', '8'), ('9', '9'), ('10', '10')
    )
	ocena = forms.ChoiceField(choices=lio, label='Ocen',  required=False)
	
