from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your models here.
# po zmianach w tym pliku: 
# python manage.py makemigrations
# python manage.py migrate

class Post(models.Model):
	title = models.CharField(max_length=120, unique=True)
	content = models.TextField(max_length=25000, blank=True, null=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	image = models.ImageField(upload_to='media',null=True,blank=True)
	author = models.CharField(max_length=120, unique=False, null=True, blank=True)
	category = models.ForeignKey('blog.Category', related_name='categories', null=True, blank=True)

	def __unicode__(self):
		return self.title

	def __str__(self):
		return self.title


class Comment(models.Model):
    post = models.ForeignKey('blog.Post', related_name='comments')
    author = models.CharField(max_length=200, blank=True)
    text = models.TextField()
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text
        
class Category(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True,)


    def approve(self):
        self.approved_category = True
        self.save()

    def __str__(self):
        return self.title
        
class Rate(models.Model):
	#jeden = 1
	#dwa = 2
	#trzy = 3
	#cztery = 4
	#piec = 5
	#szesc = 6
	#siedem = 7
	#osiem = 8
	#dziewiec = 9
	#dziesiec = 10
    #oceny = (
    #(jeden, '1'),
    #(dwa, '2'), (trzy, '3'), (cztery, '4'),  (piec, '5'), (szesc, '6'),(siedem, '7'),  (osiem, '8'), (dziewiec, '9'), (dziesiec, '10')
    #)
	post = models.ForeignKey('blog.Post', related_name='oceny')
	ocena = models.IntegerField(unique=False)
	author = models.CharField(max_length=120, unique=False, null=True, blank=True)

	def __unicode__(self):
		return self.ocena

	def __str__(self):
		return self.ocena
