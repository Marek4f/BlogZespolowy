from django.shortcuts import render, get_object_or_404
from blog.models import Post, Rate
from blog.forms import *
from .forms import  CommentForm, Oceny
from decimal import Decimal, ROUND_UP
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.

def przepisy(request):
	przepiski = Post.objects.all()
	paginator = Paginator(przepiski, 4)
	page = request.GET.get('page')
	try:
		przepisy = paginator.page(page)
	except PageNotAnInteger:
		przepisy = paginator.page(1)
	except EmptyPage:
		przepisy = paginator.page(paginator.num_pages)
	return render(request, 'przepisy.html', { 'przepisy' : przepisy })

def post(request, post_id):
	oc=Oceny()
	blok=0
	i=0
	j=0
	postt = get_object_or_404(Post, pk=post_id)
	postsr = Rate.objects.values_list('ocena', flat=True).filter(post=post_id)
	authorsr = Rate.objects.values_list('author', flat=True).filter(post=post_id)
	i=len(authorsr)
	print i
	while j<i:
		if str(authorsr[j]) in str(request.user):
			blok=1
			break
		j=j+1	
	if sum(postsr)!=0:
		sr=Decimal(sum(postsr))/Decimal(len(postsr))
		sr= (Decimal(sr).quantize(Decimal('.01'), rounding=ROUND_UP))
	else:
		sr=0
	post = Post.objects.get(id=post_id)
	if request.method == "POST":
		form = Oceny(request.POST)
		if form.is_valid():
			o=request.POST.get('ocena')
			print o
			ocenka = Rate()
			ocenka.post=postt
			ocenka.ocena=int(o)
			ocenka.author=request.user
			ocenka.save()
			postsr = Rate.objects.values_list('ocena', flat=True).filter(post=post_id)
			if sum(postsr)!=0:
				sr=Decimal(sum(postsr))/Decimal(len(postsr))
				sr= (Decimal(sr).quantize(Decimal('.01'), rounding=ROUND_UP))
			else:
				sr=0
	return render(request, 'post.html', { 'post' : post, 'oc' : oc , 'sr' : sr , 'blok' : blok })

	
def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.author=request.user
            comment.save()
            return render(request, 'post.html', { 'post' : post })
    else:
        form = CommentForm()
    return render(request, 'blog/add_comment_to_post.html', {'form': form})
