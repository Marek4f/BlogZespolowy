from django.conf.urls import url
from blog import views as blog_views
from django.conf.urls import url, include

urlpatterns = [
	url(r'^$', blog_views.przepisy),
	url(r'^(?P<post_id>\d+)/$', blog_views.post, ),
	url(r'^post/(?P<pk>\d+)/comment/$', blog_views.add_comment_to_post, name='add_comment_to_post'),
]

